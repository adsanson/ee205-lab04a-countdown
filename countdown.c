///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Albert D'Sanson <adsanson@hawaii.edu>
// @date   07_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "time.h"

int main(){
int cyear;
int ryear = 2030;
int year;
int day;
int hour;
int min;
int sec;

time_t current;
time_t reference;

struct tm *cur;
struct tm ref = {
   .tm_year = ryear - 1900,
   .tm_mday = 1, 
   .tm_mon = 0,
   .tm_hour = 0,
   .tm_min = 0,
   .tm_sec = 0
      };

   reference = mktime(&ref);
   printf("Reference time: %s\n", asctime(&ref));
   
   while(1){
   time(&current);
   cur = localtime(&current);
   double difference = difftime(reference, current);
   if(difference < 0){
      difference = difftime(reference, current);
   }
   else{
      if(ryear % 400 != 0){
      if(ryear % 4 == 1){
         difference -= 3600 * 6;
      }
      else if(ryear % 4 == 3){
         difference -= 3600 * 3 * 6;
      }
      }
   }
   year = difference / (31557600);
   day = (difference - 31557600*year)/(86400);
   hour = (difference - 31557600*year - 86400*day)/(3600);
   min = (difference - 31557600*year - 86400*day - 3600*hour)/(60);
   sec = (difference - 31557600*year - 86400*day - 3600*hour - 60*min);
 
   printf("Years: %d  Days: %d  Hours: %d  Minutes: %d  Seconds: %d\n", year, day, hour, min, sec);
   sleep(1);
   }
   return 0;
}
